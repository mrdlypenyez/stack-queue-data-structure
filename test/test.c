#include "../src/queue/QueueDataStructure.h"
#include <stdio.h>
#include <stdlib.h>

//queue prototypes
boolean queueTest();
queue *newQueue();
boolean enqueueTest(queue *q);

int main()
{
    if (!queueTest())
        return 0;

    printf("All tests passed!");
}

queue *newQueue()
{
    return (queue *)malloc(sizeof(queue));
}

boolean queueTest()
{
    queue *q = newQueue();
    initialize_queue(q, 3);
    return enqueueTest(q);
}

boolean enqueueTest(queue *q)
{
    enqueue(q, 3);
    enqueue(q, 5);
    return (q->rear->data == 5 && q->front->data == 3);
}