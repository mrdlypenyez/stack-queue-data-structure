//
// Created by Zeynep on 23.04.2021.
//

#ifndef STACK_QUEUE_DATA_STRUCTURE_STACKDATASTRUCTURE_H
#define STACK_QUEUE_DATA_STRUCTURE_STACKDATASTRUCTURE_H
#include "../DataStructureDefinitions.h"

typedef struct
{
    struct node *top;
    int cnt;
    int max_size;
} stack;

//function
void initialize_stack(stack *stk, int max_size);
void push(stack *stk, int x);
boolean isFull(stack *stk);
boolean isEmpty(stack *stk);
boolean pop(stack *stk, int *buffer);
boolean peek(stack *s, int *buffer);

#endif //STACK_QUEUE_DATA_STRUCTURE_STACKDATASTRUCTURE_H
