//
// Created by Zeynep on 23.04.2021.
//

#include "StackDataStructure.h"
#include <stdlib.h>
#include <stdio.h>

boolean isEmpty(stack *stk)
{
    return (stk->cnt == 0);
}
void initialize_stack(stack *stk, int max_size)
{
    stk->top = NULL;
    stk->cnt = 0;
    stk->max_size = max_size;
}

boolean isFull(stack *stk)
{
    return (stk->cnt == stk->max_size);
}

void push(stack *stk, int x)
{
    if (!isFull(stk))
    {
        struct node *temp = (struct node *)malloc(sizeof(struct node));
        temp->data = x;
        temp->next = stk->top;
        stk->top = temp;
        stk->cnt++;
    }
    else
    {
        printf("stack is full.\n");
    }
}

boolean pop(stack *stk, int *buffer)
{
    if (!isEmpty(stk))
    {
        struct node *temp = stk->top;
        stk->top = stk->top->next;
        *buffer = temp->data;
        free(temp);
        stk->cnt--;
        return true;
    }
    return false;
}

boolean peek(stack *s, int *buffer)
{
    if (!isEmpty(s))
    {
        struct node *temp = s->top;
        *buffer = temp->data;
        return true;
    }
    return false;
}