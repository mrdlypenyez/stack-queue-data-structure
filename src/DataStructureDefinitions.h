#ifndef DataStructureDefinitions
#define DataStructureDefinitions

struct node
{
    int data;
    struct node *next;
};

typedef enum
{
    false,
    true
} boolean;

#endif