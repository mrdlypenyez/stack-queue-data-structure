#include "QueueDataStructure.h"
#include <stdlib.h>
#include <stdio.h>

void initialize_queue(queue *qe, int max_size)
{
    qe->cnt = 0;
    qe->front = qe->rear = NULL;
    qe->max_size = max_size;
}
boolean isEmpty_queue(queue *qe)
{
    return (qe->cnt == 0);
}
boolean isFull_queue(queue *qe)
{
    return (qe->cnt == qe->max_size);
}

void enqueue(queue *q, int d)
{
    if (!isFull_queue(q))
    {
        struct node *temp = (struct node *)malloc(sizeof(struct node));
        temp->next = NULL;
        temp->data = d;
        if (isEmpty_queue(q))
        {
            q->front = q->rear = temp;
        }
        else
        {
            q->rear->next = temp;
            q->rear = temp;
        }
        q->cnt++;
        // printf("enqueue success.");
    }
    else
    {
        printf("error queue add.");
    }
}

boolean dequeue(queue *q, int *buffer)
{
    if (!isEmpty_queue(q))
    {
        struct node *temp = q->front;
        *buffer = temp->data;
        q->front = temp->next;
        free(temp);
        q->cnt--;
        return true;
    }
    return false;
}
boolean peek_queue(queue *q, int *buffer)
{
    if (!isEmpty_queue(q))
    {
        struct node *temp = q->front;
        *buffer = temp->data;
        return true;
    }
    return false;
}