//
// Created by Zeynep on 23.04.2021.
//

#ifndef STACK_QUEUE_DATA_STRUCTURE_OUEUEDATASTRUCTURE_H
#define STACK_QUEUE_DATA_STRUCTURE_OUEUEDATASTRUCTURE_H
#include "../DataStructureDefinitions.h"

typedef struct
{
    int cnt;
    struct node *front;
    struct node *rear;
    int max_size;
} queue;

//func
void initialize_queue(queue *qe, int max_size);
void enqueue(queue *q, int d);
boolean isFull_queue(queue *qe);
boolean isEmpty_queue(queue *qe);
boolean dequeue(queue *q, int *buffer);
boolean peek_queue(queue *q, int *buffer);

#endif //STACK_QUEUE_DATA_STRUCTURE_OUEUEDATASTRUCTURE_H
