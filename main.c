#include "./src/stack/StackDataStructure.h"
#include "./src/queue/QueueDataStructure.h"
#include <stdio.h>
#include <stdlib.h>

int main()
{
    stack s;
    queue q;
    initialize_queue(&q, 3);
    initialize_stack(&s, 5);
    int choice, stkCh, pk, pp, qeCh, buffer;
    push(&s, 8);
    push(&s, 3);
    enqueue(&q, 1);
    printf("Enter the choice\n\n");
    printf("Struct[1]\nQueue[2]\n");
    scanf("%d", &choice);
    switch (choice)
    {
    case 1:
        printf("Stack Operation\n");
        printf("Push[0]\nPop[1]\nPeek[2]\n");
        scanf("%d", &stkCh);
        switch (stkCh)
        {
        case 0:
            push(&s, 4);
            push(&s, 7);
            push(&s, 2);
            push(&s, 9);
            break;
        case 1:
            pop(&s, &buffer);
            pp = buffer;
            printf("Stack top value before deleting: %d", pp);
            break;
        case 2:
            peek(&s, &buffer);
            pk = buffer;
            printf("Stack top value: %d", pk);
            break;
        }
        break;
    case 2:
        printf("Queue Operation\n");
        printf("Add[0]\nDelete[1]\nPeek[2]\n");
        scanf("%d", &qeCh);
        switch (qeCh)
        {
        case 0:
            printf("Adding Queue: 7-6-9");
            enqueue(&q, 7);
            enqueue(&q, 6);
            enqueue(&q, 9);
            break;
        case 1:
            dequeue(&q, &buffer);
            printf("Deleting Queue Front value: %d", buffer);
            break;
        case 2:
            peek_queue(&q, &buffer);
            printf("Front peek value:%d", buffer);
            break;
        }
    default:
        printf("Invalid choice.Try Again!");
    }

    return 0;
}