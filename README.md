# Stack/Queue Data Structure
## Whats Purpose?

Includes repo, stack and queue implementation work.These implementations are done using linked list approach.

## Which methods were used?

### Stack
Fundamental functions such as push, pop, peek are used on the stack. In addition to these, isFull and isEmpty serve as auxiliary functions.You can access the stack files from the path `src / stack / ...  `

- All methods time complexity O(1).

| Operation  | Description                                                             |
|------------|-------------------------------------------------------------------------|
| push(item) | Pushes item on the "top" of the stack                                   |
| pop()      | Displays and removes the most recently added item from the stack.       |
| peek()     | Accesses the most recently added item in the stack, without removing it |

- Stack methods are implemented but not yet tested.


### Queue
Basic functions such as enqueue,dequeue are used on the queue. In addition to these, isFull () and isEmpty () serve as auxiliary functions.You can access the stack files from the path `src /queue / ...  `

- All methods time complexity O(1).

| Operation     | Description                                                       |
|---------------|-------------------------------------------------------------------|
| enqueue(item) | Add item at the end of the queue                                  |
| dequeue()     | Removes the item at the front of the queue                        |
| peek_queue()  |  Accesses the item at the front of the queue, without removing it |

- It passed all tests applied to queuing methods.

## Using

Creating a new instance
```c
    stack s;
    queue q;
    initialize_queue(&q, 3);
    initialize_stack(&s, 5);
```

Adding a new element

```c
push(&s, 8);
enqueue(&q, 1);
```

Removing element

```c
pop(&s, &buffer);
dequeue(&q, &buffer);
```

Peek element
```c
peek(&s, &buffer);
peek_queue(&q, &buffer);

```



